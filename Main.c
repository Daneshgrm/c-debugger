#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define DATA_NUM 17




enum data {INT , INTs , CHAR , CHARs , LONG , SHORT , FLOAT , DOUBLE, BOOL , BOOLs , IF , ELSE_IF , ELSE , SWITCH , FOR , WHILE , DO};

char* type[DATA_NUM]={"int" , "int*", "char" ,"char*", "long", "short", "float", "double" , "bool", "bool*" , "if" , "else if" , "else" , "switch" , "for" , "while" , "do"};
char temp_str[10];
int data_num[DATA_NUM] = {0};

int test = 0;
int brace_ctr = 0;
int pointto;
char** brace_point_open;
char** brace_point_close;

char check_prnth(char* str);
bool check_prnth_special(char* str);
void check_brace(char** str);
bool check_semicolon(char* str);
void first_word(const char* str);
int find_type(char* str1);
void data_numbers(int a , char* str);
void error_func(char* str);
bool check_func(char* str);

int main() {
	
	FILE *fp;
	char *string[1000];
	int counter = 0;
	
	for(int i = 0 ; i <1000 ; i++)
		string[i] = (char*)malloc(200*sizeof(char));
	fp = fopen("code.txt" , "r");	
	while(!feof(fp)){
		fgets(string[counter] , 200 ,fp);
		counter++;
	}
	for(int i = 0 ; i < counter ; i++){
		check_brace(string+i);
		first_word(string[i]);
		switch(find_type(temp_str)){
			case INT:
			case INTs:
			case CHAR:
			case CHARs:
			case LONG:
			case SHORT:
			case DOUBLE:
			case FLOAT:
			case BOOL:
			case BOOLs:
				if(!check_func(string[i])){
					data_numbers(find_type(temp_str) , string[i]);
					if(!check_semicolon(string[i]))
						printf("Line %d : ;\n" , i+1);
				}
				else{
					if(check_prnth(string[i]))
						printf("Line %d : %c\n" , i+1 , check_prnth(string[i]));
					while(*string[i+1]){
							if(*string[i+1] == '{')
								break;
							else if(*string[i+1] == '\n'){
								if(!check_semicolon(string[i]))
									printf("Line %d : ;\n" , i+1);
								break;
							}
							string[i+1]++;
						}
				}
				break;
			case IF:
			case ELSE_IF:
			case ELSE:
			case WHILE:
			case FOR:
			case SWITCH:
			case DO:
				check_brace(string+i);
				if(check_prnth(string[i]))
					printf("Line %d : %c\n" , i+1 , check_prnth(string[i]));
				else if(check_prnth_special(string[i]))
					printf("You have forgotten () in line %d \n" , i+1 );
				data_num[find_type(temp_str)]++;
				break;
			default:
				check_brace(string+i);
				if(check_prnth(string[i]))
					printf("Line %d : %c\n" , i+1 , check_prnth(string[i]));
				if(!check_semicolon(string[i]))
					printf("Line %d : ;\n" , i+1);
				break;		
		}
	}
	if(brace_ctr < 0)
		printf("You have forgotten { \n");
	else if(brace_ctr > 0)
		printf("You have forgotten } in line %d\n" , brace_point_close - &string[0]);
	printf("-------------------------\n");
	for(int i = 0 ; i < DATA_NUM ; i++)
		printf("%s -> %d\n" , type[i] , data_num[i]);
	return 0;
}
int find_type(char* str1){
	int ctr = 0;
	int flag = 0;
	for( ctr = 0 ; ctr < DATA_NUM ; ctr++)
		if(!strcmp(str1 , type[ctr])){
			flag = 1;
			break;
		}
	if(flag)
		return ctr;
	else
		return -1;
}
char check_prnth(char* str){
	int ctr = 0;
	int flag = 0;
	while(*str){
		if(*str == 40){
			ctr++;
			str++;
		}
		if(*str == 41)
		ctr--;
		str++;
	}
	if(ctr == 0)
		return 0;
	else if (ctr < 0)
		return 40;
	else
		return 41;
}
bool check_prnth_special(char* str){
	
	char prnth_arr[100];
	int ctr = 0;
	
	if(*(str+strlen(str)-1) == 32 || *(str+strlen(str)-1) == '\n'){
		*(str+strlen(str)-1) = 0;
		return check_prnth_special(str);
	}
	if(*str == ' ' || *str == '\t'){
		str++;
		return check_prnth_special(str);
	}
	for(int i = 0 ; *str != 0 ; str++)
		if(*str == '(' || *str == ')'){
			prnth_arr[test] = *str;
			test++;
		}
	if(prnth_arr[ctr-1] != ')')
		return true;
	else if(ctr > 2 && prnth_arr[ctr-1] == ')' && prnth_arr[ctr-2] != ')')
		return true;
	else
		check_prnth(str);
		
}
bool check_semicolon(char* str){
	if(*str == '{' || *str == '}' || *str == '\n' || *str == '#')
		return true;
	if(*(str+strlen(str)-1) == 59)
		return true;
	if(*(str+strlen(str)-1) == 32 || *(str+strlen(str)-1) == '\n'){
		*(str+strlen(str)-1) = 0;
		return check_semicolon(str);
	}
	if(*str == ' ' || *str == '\t'){
		str++;
		return check_semicolon(str);
	}
	else
		return false;
}
void check_brace(char** str){
	while(**str)
	{
		if(**str == 123){
			brace_ctr++;
			brace_point_open = str;
			str++;
		}
		if(**str == 125){
			brace_ctr--;
			brace_point_close = str;
		}
		*str++;
	}
}
void first_word(const char* str){
	for(int i = 0 ; i < 10 ; i++)
		temp_str[i] = 0;
	pointto = strchr(str,32) -str ;
	if(pointto < 0)
		pointto = strchr(str,40) -str;
	if(*str == 9 || *str == 32){
		str++;
		return first_word(str);
	}
	else if(strlen(str) <5)
		strcpy(temp_str , str);
	else
	strncpy(temp_str,str,pointto);
}
void data_numbers(int a , char* str){
	int ctr1 = 0;
	int ctr2 = 0;
	while(*str){
		if(*str == 44)
			ctr1++;
		else if(*str == 42 || *str == 91)
			ctr2++;
		str++;
	}
		data_num[a]+= ctr1+1-ctr2;
		data_num[a+1]+=ctr2;
}
bool check_func(char* str){
	int flag = 0;
	while(*str){
		if(*str == 40){
			flag = 1;
			break;
		}
		str++;
	}
	if(flag == 1)
		return true;
	else
		return false;
}
